cmake_minimum_required(VERSION 2.6.2)
 
project(run)
 
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}")

#find_package(Boost 1.47.0 REQUIRED program_options system filesystem)

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

find_package( Threads )

#find_library(pstreams_LIBRARIES pstreams REQUIRED)



add_definitions(-O0 -g -Wall -funsigned-char --std=c++1y -pedantic)

include_directories("/usr/include/pstreams/")
include_directories("include")
file(GLOB_RECURSE run_SOURCES "src/**.cpp")
add_executable(run ${run_SOURCES})
#target_link_libraries(run ${Boost_LIBRARIES})
#target_link_libraries(run ${pstreams_LIBRARIES})
target_link_libraries(run ${CMAKE_THREAD_LIBS_INIT})


install (TARGETS run DESTINATION bin)

