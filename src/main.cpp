#include <fstream>
#include <iostream>
#include <chrono> 
#include <functional> 
#include <mutex>
#include <thread> 
#include <atomic> 
#include <vector> 
#include <tuple> 
#include <set> 
#include <iomanip> 
using namespace std;

#include <signal.h>
#include "pstream.h"

extern uint64_t MurmurHash64A ( const void * key, int len, unsigned int seed );

function<void()> func(nullptr);

void my_handler(int s)
{
    cout << "Caught signal " << s << ", finalizing.." << endl;
    func(); // func is assigned before the handler is registered.
    exit(1); 
}

int main (int argc, char *argv[])
{
    if (argc == 1) {
        cout << "Usage: " << argv[0] << " <<command here>>" << endl;
        return 1;
    }
    string command;
    for (int i=1; i<argc; i++) {
        if (i > 1) command.append(" ");
        command.append(argv[i]);
    }

    vector< tuple<uint64_t, double, double> > hash_duration;      // <hash>, <elapsed time>, <elapsed time>
    multiset< uint64_t > existing_hashes;                         // used to calculate the occurence for a given hash in current run.
    vector< tuple<uint64_t, double, double> > hash_duration_hist; // <hash>, <remaining until next line>, <remaining time total>

    ifstream inputfile;
    inputfile.open("/tmp/test.bin", std::ifstream::binary);
    while (true) {
        uint64_t hash = 0;
        double duration = 0;
        double duration_total = 0;
        inputfile.read(reinterpret_cast<char *>(&hash), sizeof(hash));
        if (inputfile.gcount() != sizeof(hash))
            break;
        if (inputfile.eof())
            break;
        inputfile.read(reinterpret_cast<char *>(&duration), sizeof(duration));
        inputfile.read(reinterpret_cast<char *>(&duration_total), sizeof(duration_total));
        hash_duration_hist.push_back(make_tuple(hash, duration, duration_total));
    }
    inputfile.close();

    chrono::time_point<chrono::system_clock> start, current, est_start;
    start = chrono::system_clock::now();
    redi::ipstream in(command);
    string str;
    string s;
    unsigned int seed = 1001;
    double current_est = 0;
    double current_est_total = 0;
    
    mutex mut;
    atomic<bool> done(false);

    auto display_progress_bars = [&]() {
        if (!current_est_total)
            return;
        current = chrono::system_clock::now();
        chrono::duration<double> elapsed_seconds = current - est_start;
        auto elapsed = elapsed_seconds.count();
        chrono::duration<double> elapsed_seconds_total = current - start;
        auto elapsed_total = elapsed_seconds_total.count();
        struct winsize size;
        ioctl(STDOUT_FILENO,TIOCGWINSZ, &size);
        auto cols = floor(size.ws_col / 2.0);
        cout << "\r";
        for (decltype(cols) i=0; i<cols; i++) {
            bool progress = (((elapsed_total) / (elapsed_total - elapsed + current_est_total)) * cols) < i;
            cout << (progress ? "░" : "█");
        }
        cout << " ";
        cols--;
        for (decltype(cols) i=0; i<cols; i++) {
            bool progress = ((elapsed / current_est) * cols) < i;
            cout << (progress ? "░" : "█");
        }
        cout << "\r";
        cout.flush();
    };

    thread reader( [&]() {
        while (getline(in, s)) {
            mut.lock();
            current = chrono::system_clock::now();
            mut.unlock();
            chrono::duration<double> elapsed_seconds = current - start;
            uint64_t hash = MurmurHash64A ( s.c_str(), s.size(), seed);
            existing_hashes.insert(hash);

            std::string tmp(s + " @ " + std::to_string(existing_hashes.count(hash)));
            hash = MurmurHash64A ( tmp.c_str(), tmp.size(), seed);

            mut.lock();
            struct winsize size;
            ioctl(STDOUT_FILENO,TIOCGWINSZ, &size);
            auto cols = size.ws_col;
            cout << "\r";
            for (decltype(cols) i=0; i<cols; i++) {
                cout << " ";
            }
            cout.flush();
            auto it = find_if(begin(hash_duration_hist),
                              end(hash_duration_hist),
                              [&](std::tuple<uint64_t, double, double> tupl) {
                                  return std::get<0>(tupl) == hash;
                              });
            std::string estimated;
            if (it != hash_duration_hist.end()) {
                current_est = std::get<1>(*it);
                current_est_total = std::get<2>(*it);
                est_start = current;
            } else {
              std::string s{"*"};
              std::swap(estimated, s);
            }
            cout << "\r[RUN = " << std::fixed << std::setprecision(5) << elapsed_seconds.count() << "s, ETA ~ "
                 << std::fixed << std::setprecision(2) << current_est_total << "s" << estimated << "] "
                 << s << endl;
            hash_duration.push_back(make_tuple(hash, elapsed_seconds.count(), elapsed_seconds.count()));
            display_progress_bars();
            mut.unlock();
        }
        done = true;
    });

    thread progress_display( [&]() {
        while (!done) {
            std::this_thread::sleep_for (std::chrono::milliseconds(50));
            mut.lock();
            display_progress_bars();
            mut.unlock();
        }
    });

    auto finalize = [&]() {
        // convert elapsed time into remaining time
        chrono::duration<double> total_time = current - start;
        tuple<uint64_t, double, double> *previous = nullptr;
        for (auto &tupl : hash_duration) {
            if (previous) {
                get<1>(*previous) = get<1>(tupl) - get<1>(*previous);
            }
            previous = &tupl; // maybe this is UB expecting this pointer not to change until next iteration.
            get<2>(tupl) = total_time.count() - get<2>(tupl);
        }
        if (previous) {
            get<1>(*previous) = get<2>(*previous); // last statement
        }

        sort(hash_duration.begin(), hash_duration.end(), [](auto & A, auto & B) { return get<0>(A) < get<0>(B); });
        sort(hash_duration_hist.begin(), hash_duration_hist.end(), [](auto & A, auto & B) { return get<0>(A) < get<0>(B); });

        // merge the two maps
        vector<tuple< uint64_t, double, double>> hash_duration_union;
        std::set_union(hash_duration.begin(), hash_duration.end(),
                       hash_duration_hist.begin(), hash_duration_hist.end(),
                       std::back_inserter(hash_duration_union),
                       [](auto & A, auto & B) {
                           return get<0>(A) < get<0>(B);
                       });

        // write to disk for next run
        ofstream outputfile;
        outputfile.open("/tmp/test.bin", ios::out | ios::trunc | ios::binary); 
        for (auto & tupl : hash_duration_union) {
            uint64_t hash(std::get<0>(tupl)); 
            double duration(std::get<1>(tupl)); 
            double duration_total(std::get<2>(tupl)); 
            outputfile.write(reinterpret_cast<char *>(&hash), sizeof(hash));
            outputfile.write(reinterpret_cast<char *>(&duration), sizeof(duration));
            outputfile.write(reinterpret_cast<char *>(&duration_total), sizeof(duration_total));
        }
        outputfile.close();
    };

    // set the global function for the handler
    func = std::function<void()>([&]() {
        finalize();
        exit(1); 
    });

    // register the handler
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    // wait for natural threads termination
    reader.join();
    progress_display.join();

    finalize();
}
